﻿#region Using Statements
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
#endregion

namespace Questions
{
    public class Answer
    {
        private bool isCorrect;
        private string answerText;
        private string[] answerTexts;

        /// <summary>
        /// An answer to a question.
        /// </summary>
        /// <param name="correct">Determines if it is the correct answer.</param>
        /// <param name="text">The text of the answer.</param>
        public Answer(bool correct, string text)
        {
            isCorrect = correct;
            answerText = text;
        }

        /// <summary>
        /// An answer to a question.
        /// </summary>
        /// <param name="correct">Determines if it is the correct answer.</param>
        /// <param name="text">The texts of the answer.</param>
        public Answer(bool correct, string[] text)
        {
            isCorrect = correct;
            answerTexts = text;
        }

        /// <summary>
        /// Gets and sets correct.
        /// </summary>
        public bool IsCorrect { get { return isCorrect; } set { isCorrect = value; } }
        /// <summary>
        /// Gets and sets the answers text.
        /// </summary>
        public string AnswerText { get { return answerText; } set { answerText = value; } }
        public string[] AnswerTexts { get { return answerTexts; } set { answerTexts = value; } }
    }
}
﻿#region Using Statements
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using ContentLoader;
using Microsoft.Xna.Framework;
using VideoDisplay;
#endregion

namespace Questions
{
    public class Instructions : WallOfText
    {
        public Instructions()
        {
            Text = new string[] 
                {
                    "Fahooey Quiz. You know the math, and all the stuff.",
                    "Except you are still wrong because you did not",
                    "write the answer the way your teacher wanted it.",
                    "We know how you feel. We made this quiz so that",
                    "when you take it, you will be 'guaranteed' to pass",
                    "any test if format means everything.",
                    "",
                    "So go forth, and solve the questions, my lord.",
                };
        }
    }
}

﻿#region Using Statements
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Input;
using VideoDisplay;
using Microsoft.Xna.Framework.Graphics;
using ContentLoader;
using Microsoft.Xna.Framework;
#endregion

namespace Questions.SpecialQuestions
{
    public class QuestionFinal : Question666
    {
        public QuestionFinal(string[] q, string[] a)
            : base(q, a, -5)
        {
        }

        public override void DisplayQuestion(SpriteBatch sb)
        {
            sb.Draw(TheContentLoader.Other[4],
                new Vector2((VideoVariables.WIDTH / 2) - (TheContentLoader.Other[4].Width / 2),
                    (float)(VideoVariables.HEIGHT * .25) - (TheContentLoader.Other[4].Height / 2)),
                Color.White);

            base.DisplayQuestion(sb);
        }

        public override byte ClickAnswers(MouseState mouseState)
        {
            if ((mouseState.X > 20 && mouseState.X < VideoVariables.WIDTH - 20) &&
                (mouseState.Y > VideoVariables.HEIGHT / 2 + 5 && mouseState.Y < VideoVariables.HEIGHT - 20))
            {
                return 2;
            }
            return 0;
        }

        public override void DisplayAnswers(SpriteBatch sb, MouseState mouseState)
        {
            //Upper Left Box (0)
            if ((mouseState.X > 20 && mouseState.X < VideoVariables.WIDTH - 20) &&
                (mouseState.Y > VideoVariables.HEIGHT / 2 + 5 && mouseState.Y < VideoVariables.HEIGHT - 20))
            {
                sb.Draw(TheContentLoader.SixSixSix[11],
                    new Vector2(20, VideoVariables.HEIGHT / 2 + 5),
                    Color.White);
            }
            else
            {
                sb.Draw(TheContentLoader.SixSixSix[10],
                    new Vector2(20, VideoVariables.HEIGHT / 2 + 5),
                    Color.White);
            }

            Simplify.CenterText(sb,
                TheContentLoader.Font[3],
                Answers[0].AnswerText,
                (int)((VideoVariables.WIDTH * .5)), (int)(VideoVariables.HEIGHT * .75),
                Color.White);
        }
    }
}

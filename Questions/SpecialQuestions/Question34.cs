﻿#region Using Statements
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using ContentLoader;
using Microsoft.Xna.Framework;
using VideoDisplay;
#endregion

namespace Questions.SpecialQuestions
{
    public class Question34 : Question
    {
        public Question34(string[] q, string[] a, int correctNum)
            : base(q, a, correctNum)
        {

        }
        public override void DisplayQuestion(SpriteBatch sb)
        {
            base.DisplayQuestion(sb);
            sb.Draw(TheContentLoader.Other[3],
                new Vector2((VideoVariables.WIDTH / 2) - (TheContentLoader.Other[3].Width / 2),
                    (float)(VideoVariables.HEIGHT * .25) - (TheContentLoader.Other[3].Height / 2)),
                Color.White);
        }
    }
}

﻿#region Using Statements
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Input;
#endregion

namespace Questions.SpecialQuestions
{
    public class Question45 : Question666
    {
        public Question45(string[] q, string[] a)
            : base(q, a, -5)
        {
        }

        public override byte ClickAnswers(MouseState mouseState)
        {
            byte temp = base.ClickAnswers(mouseState);

            if (temp == 1)
            {
                return 2;
            }
            return temp;
        }
    }
}

﻿#region Using Statements
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using VideoDisplay;
using Microsoft.Xna.Framework.Input;
#endregion

namespace Questions.SpecialQuestions
{
    public class Question18 : Question
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="q">The question. Each position in the array moves it down.  Do not use the backslash n.</param>
        /// <param name="a">The array of answers.</param>
        /// <param name="correctNum">Value of 0-3, whichever answer is correct. Any other indicates the boxes are all wrong.</param>
        public Question18(string[] q, string[] a, int correctNum)
            : base(q, a, correctNum)
        {

        }

        /// <summary>
        /// Returns a number indicating whether the buttons are clicked
        /// and whether or not it is correct.
        /// </summary>
        /// <param name="mouseState"></param>
        /// <returns>0 for not clicked. 1 for wrong. 2 for correct.</returns>
        public override byte ClickAnswers(MouseState mouseState)
        {
            if (mouseState.X > 20 && mouseState.X < VideoVariables.WIDTH / 2 - 20)
            {
                if (mouseState.Y > VideoVariables.HEIGHT / 2 + 5 &&
                        mouseState.Y < VideoVariables.HEIGHT * (0.75) - 20)
                {
                    return 1;
                }
                else if (mouseState.Y > VideoVariables.HEIGHT * (.75) + 5 &&
                        mouseState.Y < VideoVariables.HEIGHT - 20)
                {
                    return 1;
                }
            }
            else if (mouseState.X > VideoVariables.WIDTH / 2 + 20 && mouseState.X < VideoVariables.WIDTH - 20)
            {
                if (mouseState.Y > VideoVariables.HEIGHT / 2 + 5 &&
                        mouseState.Y < VideoVariables.HEIGHT * (0.75) - 20)
                {
                    return 2;
                }
                else if (mouseState.Y > VideoVariables.HEIGHT * (.75) + 5 &&
                        mouseState.Y < VideoVariables.HEIGHT - 20)
                {
                    return 128;
                }
            }
            return 0;
        }
    }
}

﻿#region Using Statements
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using VideoDisplay;
using ContentLoader;
#endregion

namespace Questions.SpecialQuestions
{
    public class Question17 : Question
    {
        public Question17(string[] q, string[] a)
            : base(q, a, 4)
        {
        }
        public override byte ClickAnswers(MouseState mouseState)
        {
            if ((mouseState.X > (TheContentLoader.Font[0].MeasureString("Level: ").X) + 20 && mouseState.X < (TheContentLoader.Font[0].MeasureString("Level: 1").X) + 20) &&
                            (mouseState.Y > VideoVariables.HEIGHT / 2 - 45 - TheContentLoader.Font[0].MeasureString("1").Y && mouseState.Y < VideoVariables.HEIGHT / 2 - 40))
            {
                return 2;
            }
            return base.ClickAnswers(mouseState);
        }
    }
}

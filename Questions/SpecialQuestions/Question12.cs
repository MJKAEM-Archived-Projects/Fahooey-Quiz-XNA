﻿#region Using Statements
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Input;
using VideoDisplay;
#endregion

namespace Questions.SpecialQuestions
{
    public class Question12 : Question
    {
        public Question12(string[] q, string[] a, int correctNum)
            : base(q, a, correctNum)
        {
        }
        public override byte ClickAnswers(MouseState mouseState)
        {
            if (mouseState.X > 20 && mouseState.X < VideoVariables.WIDTH / 2 - 20)
            {
                if (mouseState.Y > VideoVariables.HEIGHT / 2 + 5 &&
                        mouseState.Y < VideoVariables.HEIGHT * (0.75) - 20)
                {
                    SetQuestionText(0, "\u00BFHabla espa\u00F1ol?");
                    ChangeAnswers(
                        new string[]
                        {
                            "Io non parlo spagnolo.",
                            "No hablo ingl\u00E9s.",
                            "Je ne parle pas anglais.",
                            "In English, please."
                        });

                    return 2;
                }
                else if (mouseState.Y > VideoVariables.HEIGHT * (.75) + 5 &&
                        mouseState.Y < VideoVariables.HEIGHT - 20)
                {
                    return 1;
                }
            }
            else if (mouseState.X > VideoVariables.WIDTH / 2 + 20 && mouseState.X < VideoVariables.WIDTH - 20)
            {
                if (mouseState.Y > VideoVariables.HEIGHT / 2 + 5 &&
                        mouseState.Y < VideoVariables.HEIGHT * (0.75) - 20)
                {
                    return 1;
                }
                else if (mouseState.Y > VideoVariables.HEIGHT * (.75) + 5 &&
                        mouseState.Y < VideoVariables.HEIGHT - 20)
                {
                    SetQuestionText(0, "Do you speak Spanish?");
                    ChangeAnswers(
                        new string[]
                        {
                            "I don't speak Spanish.",
                            "I don't speak English.",
                            "I don't speak English.",
                            "Doh!"
                        });

                    return 1;
                }
            }
            return 0;
        }
    }
}

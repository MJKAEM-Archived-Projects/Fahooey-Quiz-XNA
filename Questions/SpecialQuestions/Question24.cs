﻿#region Using Statements
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using ContentLoader;
using VideoDisplay;
using Microsoft.Xna.Framework;
#endregion

namespace Questions.SpecialQuestions
{
    public class Question24 : Question
    {
        public Question24(string[] q, string[] a, int correctNum)
            : base(q, a, correctNum)
        {
        }
        public override void DisplayQuestion(SpriteBatch sb)
        {
            for (int i = QuestionText.Length - 1; i >= 0; --i)
            {
                Simplify.CenterText(sb,
                    TheContentLoader.Font[5],
                    QuestionText[i],
                    VideoVariables.WIDTH / 2, 50 + (50 * i),
                    Color.Black);
            }
        }
    }
}

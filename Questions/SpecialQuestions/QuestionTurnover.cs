﻿#region Using Statements
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Input;
using VideoDisplay;
using Microsoft.Xna.Framework.Graphics;
using ContentLoader;
using Microsoft.Xna.Framework;
#endregion

namespace Questions.SpecialQuestions
{
    public class QuestionTurnover : Question
    {
        public QuestionTurnover(string[] q, string[] a)
            : base(q, a, -5)
        {
        }

        public override byte ClickAnswers(MouseState mouseState)
        {
            if ((mouseState.X > 20 && mouseState.X < VideoVariables.WIDTH - 20) &&
                (mouseState.Y > VideoVariables.HEIGHT / 2 + 5 && mouseState.Y < VideoVariables.HEIGHT - 20))
            {
                return 2;
            }
            return 0;
        }

        public override void DisplayAnswers(SpriteBatch sb, MouseState mouseState)
        {
            //Upper Left Box (0)
            if ((mouseState.X > 20 && mouseState.X < VideoVariables.WIDTH - 20) &&
                (mouseState.Y > VideoVariables.HEIGHT / 2 + 5 && mouseState.Y < VideoVariables.HEIGHT - 20))
            {
                sb.Draw(TheContentLoader.Normal[11],
                    new Vector2(20, VideoVariables.HEIGHT / 2 + 5),
                    Color.White);
            }
            else
            {
                sb.Draw(TheContentLoader.Normal[10],
                    new Vector2(20, VideoVariables.HEIGHT / 2 + 5),
                    Color.White);
            }

            Simplify.CenterText(sb,
                TheContentLoader.Font[1],
                Answers[0].AnswerText,
                (int)((VideoVariables.WIDTH * .5)), (int)(VideoVariables.HEIGHT * .75),
                Color.Black);
        }
    }
}

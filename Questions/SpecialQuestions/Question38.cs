﻿#region Using Statements
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Graphics;
#endregion

namespace Questions.SpecialQuestions
{
    public class Question38 : Question
    {
        private int wait;

        public Question38(string[] q, string[] a)
            : base(q, a, 4)
        {
            wait = 1 * 60;
        }

        public void AdvanceQuestion(KeyboardState keyboardState, KeyboardState oldKeyboardState)
        {
            if (ProcessAnswer(keyboardState, oldKeyboardState))
            {
                if (QuestionText[3].Equals(""))
                {
                    SetQuestionText(3, "A");
                }
                else if (QuestionText[3].Equals("A"))
                {
                    SetQuestionText(3, "An");
                }
                else if (QuestionText[3].Equals("An"))
                {
                    SetQuestionText(3, "Ann");
                }
                else if (QuestionText[3].Equals("Ann"))
                {
                    SetQuestionText(3, "Annu");
                }
                else if (QuestionText[3].Equals("Annu"))
                {
                    SetQuestionText(3, "Annui");
                }
                else if (QuestionText[3].Equals("Annui"))
                {
                    SetQuestionText(3, "Annuit");
                }
                else if (QuestionText[3].Equals("Annuit"))
                {
                    SetQuestionText(3, "Annuity");
                }
            }
        }
        public bool ProcessAnswer(KeyboardState keyboardState, KeyboardState oldKeyboardState)
        {
            if (QuestionText[3].Equals(""))
            {
                return (keyboardState.IsKeyUp(Keys.A) && oldKeyboardState.IsKeyDown(Keys.A));
            }
            else if (QuestionText[3].Equals("A"))
            {
                return (keyboardState.IsKeyUp(Keys.N) && oldKeyboardState.IsKeyDown(Keys.N));
            }
            else if (QuestionText[3].Equals("An"))
            {
                return (keyboardState.IsKeyUp(Keys.N) && oldKeyboardState.IsKeyDown(Keys.N));
            }
            else if (QuestionText[3].Equals("Ann"))
            {
                return (keyboardState.IsKeyUp(Keys.U) && oldKeyboardState.IsKeyDown(Keys.U));
            }
            else if (QuestionText[3].Equals("Annu"))
            {
                return (keyboardState.IsKeyUp(Keys.I) && oldKeyboardState.IsKeyDown(Keys.I));
            }
            else if (QuestionText[3].Equals("Annui"))
            {
                return (keyboardState.IsKeyUp(Keys.T) && oldKeyboardState.IsKeyDown(Keys.T));
            }
            else if (QuestionText[3].Equals("Annuit"))
            {
                return (keyboardState.IsKeyUp(Keys.Y) && oldKeyboardState.IsKeyDown(Keys.Y));
            }
            else
            {
                return false;
            }
        }
        public override void DisplayAnswers(SpriteBatch sb, MouseState mouseState)
        {
            base.DisplayAnswers(sb, mouseState);

            if (QuestionText[3].Equals("Annuity"))
            {
                --wait;
            }
        }

        public bool GetProcessWait()
        {
            if (wait == 0)
            {
                wait = 1 * 60;
                SetQuestionText(3, "");
                return true;
            }
            return false;
        }
    }
}

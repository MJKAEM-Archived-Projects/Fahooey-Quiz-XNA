﻿#region Using Statements
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Input;
#endregion

namespace Questions.SpecialQuestions
{
    public class Question28 : Question
    {
        private byte jingle1, jingle2, jingle3;

        public Question28(string[] q, string[] a)
            : base(q, a, 1)
        {
        }
        public override byte ClickAnswers(MouseState mouseState)
        {
            byte ans = base.ClickAnswers(mouseState);

            if (ans == 2)
            {
                if (jingle1 != 111)
                {
                    if (jingle1 / 1 == 0)
                    {
                        ++jingle1;
                        Answers[1].IsCorrect = false;
                        Answers[2].IsCorrect = true;
                        SetQuestionText(2, QuestionText[2] + "sin ");
                    }
                    else if (jingle1 / 10 == 0)
                    {
                        jingle1 += 10;

                        SetQuestionText(2, QuestionText[2] + "cos ");
                    }
                    else if (jingle1 / 100 == 0)
                    {
                        jingle1 += 100;
                        Answers[2].IsCorrect = false;
                        Answers[1].IsCorrect = true;
                        SetQuestionText(2, QuestionText[2] + "cos ");
                    }
                }
                else if (jingle2 != 111)
                {
                    if (jingle2 / 1 == 0)
                    {
                        ++jingle2;
                        Answers[1].IsCorrect = false;
                        Answers[0].IsCorrect = true;
                        SetQuestionText(2, QuestionText[2] + "sin");
                    }
                    else if (jingle2 / 10 == 0)
                    {
                        jingle2 += 10;
                        Answers[0].IsCorrect = false;
                        Answers[3].IsCorrect = true;
                        SetQuestionText(2, "sin\u03B1 cos cos sin");
                    }
                    else if (jingle2 / 100 == 0) //{"\u03B1", "sine", "cosine", "\u03B2"}
                    {
                        jingle2 += 100;
                        Answers[3].IsCorrect = false;
                        Answers[0].IsCorrect = true;
                        SetQuestionText(2, "sin\u03B1 cos\u03B2 cos sin");
                    }
                }
                else if (jingle3 != 111)
                {
                    if (jingle3 / 1 == 0)
                    {
                        ++jingle3;
                        Answers[0].IsCorrect = false;
                        Answers[3].IsCorrect = true;
                        SetQuestionText(2, "sin\u03B1 cos\u03B2 cos\u03B1 sin");
                    }
                    else if (jingle3 / 10 == 0)
                    {
                        jingle3 += 10;

                        Answers[3].IsCorrect = false;
                        Answers[2].IsCorrect = true;

                        SetQuestionText(2, "sin\u03B1 cos\u03B2 ?? cos\u03B1 sin\u03B2");
                        ChangeAnswers(new string[] {
							"Sine stays the same.",
							"Sign changes.",
							"Sign stays the same.",
							"Sine changes."
					});
                    }
                    else if (jingle3 / 100 == 0)
                    {
                        jingle1 = 0;
                        jingle2 = 0;
                        jingle3 = 0;
                        ChangeAnswers(new string[] { "\u03B1", "sine", "cosine", "\u03B2" });
                        SetQuestionText(2, "");

                        Answers[2].IsCorrect = false;
                        Answers[1].IsCorrect = true;

                        return 2;
                    }
                }
            }
            else if (ans == 1)
            {
                return 1;
            }

            return 0;
        }
    }
}

﻿#region Using Statements
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;
using ContentLoader;
using VideoDisplay;
#endregion

namespace Questions.SpecialQuestions
{
    public class Question25 : Question
    {
        public Question25(string[] a, int correctNum)
            : base(null, a, correctNum)
        {
        }
        public override void DisplayQuestion(SpriteBatch sb)
        {
            sb.Draw(TheContentLoader.Other[2],
                new Vector2(
                    (VideoVariables.WIDTH / 2) - (TheContentLoader.Other[2].Width / 2),
                    (float)(VideoVariables.HEIGHT * .25) - (TheContentLoader.Other[2].Height / 2)),
                Color.White);
        }
    }
}

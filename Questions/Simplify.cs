﻿#region Using Statements
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;
using System.Diagnostics.Contracts;
using ContentLoader;
#endregion

namespace Questions
{
    public static class Simplify
    {
        public static void LeftText(SpriteBatch sb, SpriteFont sf, string text, float x, float y, Color c)
        {
            Vector2 measure = sf.MeasureString(text);

            sb.DrawString(sf, text, new Vector2(x, (int)(y - (measure.Y) + measure.Y / 4)), c);
        }
        public static void CenterText(SpriteBatch sb, SpriteFont sf, string text, float x, float y, Color c)
        {
            Vector2 measure = sf.MeasureString(text);

            sb.DrawString(sf,
                text,
                new Vector2((int)(x - (measure.X / 2)), (int)(y - (measure.Y) + (measure.Y / 4))),
                c);

            /*case RIGHT:
                sb.DrawString(sf, text, new Vector2(x - sf.MeasureString(text).X, y), c);
                break;*/
            //Unused
        }
        public static void CenterText(SpriteBatch sb, SpriteFont sf, string[] text, float x, float y, Color c)
        {
            for (int i = 0; i < text.Length; ++i)
            {
                Vector2 measure = sf.MeasureString(text[i]);

                sb.DrawString(sf,
                    text[i],
                    new Vector2((int)(x - (measure.X / 2)), (int)(y - (measure.Y) + (measure.Y / 4) + (measure.Y * i))),
                    c);

            }
            /*case RIGHT:
                sb.DrawString(sf, text, new Vector2(x - sf.MeasureString(text).X, y), c);
                break;*/
            //Unused
        }
    }
}

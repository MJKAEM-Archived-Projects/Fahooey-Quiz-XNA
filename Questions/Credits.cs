﻿#region Using Statements
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
#endregion

namespace Questions
{
    public class Credits : WallOfText
    {
        /// <summary>
        /// Creates the credits for the game.
        /// </summary>
        /// <param name="trueTheme">Type trueTheme.</param>
        public Credits(bool trueTheme)
        {
            if (trueTheme)
            {
                Text = new string[] 
                {
                    "This game was made by a lunatic.",
                    "",
                    "Sounds stolen from:",
                    "Soundbible.com; Soundjax.com; Civil Defense Alarm",
                    "",
                    "Songs:",
                    "I Ain't Your Antichrist, I'm Your Uncle Sam",
                    "by Gunther the Clown",
                    "",
                    "Boss Theme from Sonic CD (US version)",
                };
            }
            else
            {
                Text = new string[] 
                {
                    "This game was made by a lunatic.",
                    "",
                    "Sounds stolen from:",
                    "Soundbible.com; Soundjax.com; Civil Defense Alarm",
                    "",
                    "Songs:",
                    "Didgeridoo",
                    "http://www.youtube.com/watch?v=zu4pWM-nRq8",
                    "",
                    "Boss Theme from Sonic CD (US version)",
                };
            }
        }
    }
}

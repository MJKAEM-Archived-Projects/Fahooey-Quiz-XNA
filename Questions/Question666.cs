﻿#region Using Statements
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using VideoDisplay;
using Microsoft.Xna.Framework;
using ContentLoader;
#endregion

namespace Questions
{
    public class Question666 : Question
    {
        public Question666(string[] q, string[] a, int correctNum)
            : base(q, a, correctNum)
        {

        }
        public Question666(string[] q, string[][] a, int correctNum)
            : base(q, a, correctNum)
        {

        }

        public override void DisplayQuestion(SpriteBatch sb)
        {
            for (int i = QuestionText.Length - 1; i >= 0; --i)
            {
                Simplify.CenterText(sb,
                    TheContentLoader.Font[3],
                    QuestionText[i],
                    VideoVariables.WIDTH / 2, 50 + (50 * i),
                    Color.White);
            }
        }

        public override void DisplayAnswers(SpriteBatch sb, MouseState mouseState)
        {
            #region Upper Left Box (0)
            if ((mouseState.X > 20 && mouseState.X < VideoVariables.WIDTH / 2 - 20) &&
                (mouseState.Y > VideoVariables.HEIGHT / 2 + 5 && mouseState.Y < VideoVariables.HEIGHT * (0.75) - 20))
            {
                sb.Draw(TheContentLoader.SixSixSix[1],
                    new Vector2(20, VideoVariables.HEIGHT / 2 + 5),
                    Color.White);
            }
            else
            {
                sb.Draw(TheContentLoader.SixSixSix[0],
                    new Vector2(20, VideoVariables.HEIGHT / 2 + 5),
                    Color.White);
            }
            if (Answers[0].AnswerText != null)
            {
                Simplify.CenterText(sb,
                    TheContentLoader.Font[2],
                    Answers[0].AnswerText,
                    (float)(VideoVariables.WIDTH *.25), (VideoVariables.HEIGHT / 2 + 5) + 50,
                    Color.White);
            }
            else
            {
                Simplify.CenterText(sb,
                    TheContentLoader.Font[2],
                    Answers[0].AnswerTexts,
                    (float)(VideoVariables.WIDTH *.25), (VideoVariables.HEIGHT / 2 + 5) + 50,
                    Color.White);
            }
            #endregion
            #region Lower Left Box (1)
            if ((mouseState.X > 20 && mouseState.X < VideoVariables.WIDTH / 2 - 20) &&
                (mouseState.Y > VideoVariables.HEIGHT * (.75) + 5 && mouseState.Y < VideoVariables.HEIGHT - 20))
            {
                sb.Draw(TheContentLoader.SixSixSix[1],
                    new Vector2(20, (float)(VideoVariables.HEIGHT * (.75) + 5)),
                    Color.White);
            }
            else
            {
                sb.Draw(TheContentLoader.SixSixSix[0],
                    new Vector2(20, (float)(VideoVariables.HEIGHT * (.75) + 5)),
                    Color.White);
            }

            if (Answers[1].AnswerText != null)
            {
                Simplify.CenterText(sb,
                    TheContentLoader.Font[2],
                    Answers[1].AnswerText,
                    (float)(VideoVariables.WIDTH *.25), (float)(VideoVariables.HEIGHT * .75 + 5) + 50,
                    Color.White);
            }
            else
            {
                Simplify.CenterText(sb,
                    TheContentLoader.Font[2],
                    Answers[1].AnswerTexts,
                    (float)(VideoVariables.WIDTH *.25), (float)(VideoVariables.HEIGHT * .75 + 5) + 50,
                    Color.White);
            }
            #endregion
            #region Upper Right Box (2)
            if ((mouseState.X > VideoVariables.WIDTH / 2 + 20 && mouseState.X < VideoVariables.WIDTH - 20) &&
                (mouseState.Y > VideoVariables.HEIGHT / 2 + 5 && mouseState.Y < VideoVariables.HEIGHT * (0.75) - 20))
            {
                sb.Draw(TheContentLoader.SixSixSix[1],
                    new Vector2(VideoVariables.WIDTH / 2 + 20, VideoVariables.HEIGHT / 2 + 5),
                    Color.White);
            }
            else
            {
                sb.Draw(TheContentLoader.SixSixSix[0],
                    new Vector2(VideoVariables.WIDTH / 2 + 20, VideoVariables.HEIGHT / 2 + 5),
                    Color.White);
            }
            if (Answers[2].AnswerText != null)
            {
                Simplify.CenterText(sb,
                    TheContentLoader.Font[2],
                    Answers[2].AnswerText,
                    (float)(VideoVariables.WIDTH * .75), (VideoVariables.HEIGHT / 2 + 5) + 50,
                    Color.White);
            }
            else
            {
                Simplify.CenterText(sb,
                    TheContentLoader.Font[2],
                    Answers[2].AnswerTexts,
                    (float)(VideoVariables.WIDTH * .75), (VideoVariables.HEIGHT / 2 + 5) + 50,
                    Color.White);
            }
            #endregion
            #region Lower Right Box (3)
            if ((mouseState.X > VideoVariables.WIDTH / 2 + 20 && mouseState.X < VideoVariables.WIDTH - 20) &&
                (mouseState.Y > VideoVariables.HEIGHT * (.75) + 5 && mouseState.Y < VideoVariables.HEIGHT - 20))
            {
                sb.Draw(TheContentLoader.SixSixSix[1],
                    new Vector2(VideoVariables.WIDTH / 2 + 20, (float)(VideoVariables.HEIGHT * (.75) + 5)),
                    Color.White);
            }
            else
            {
                sb.Draw(TheContentLoader.SixSixSix[0],
                    new Vector2(VideoVariables.WIDTH / 2 + 20, (float)(VideoVariables.HEIGHT * (.75) + 5)),
                    Color.White);
            }
            if (Answers[3].AnswerText != null)
            {
                Simplify.CenterText(sb,
                    TheContentLoader.Font[2],
                    Answers[3].AnswerText,
                    (float)(VideoVariables.WIDTH * .75), (float)(VideoVariables.HEIGHT * .75 + 5) + 50,
                    Color.White);
            }
            else
            {
                Simplify.CenterText(sb,
                    TheContentLoader.Font[2],
                    Answers[3].AnswerTexts,
                    (float)(VideoVariables.WIDTH * .75), (float)(VideoVariables.HEIGHT * .75 + 5) + 50,
                    Color.White);
            }
            #endregion
        }
    }
}

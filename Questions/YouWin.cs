﻿#region Using Statements
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Input;
using VideoDisplay;
using Microsoft.Xna.Framework.Graphics;
using ContentLoader;
using Microsoft.Xna.Framework;
#endregion

namespace Questions
{
    public class YouWin : WallOfText
    {
        public const bool debugMode = false;

        private bool legit;

        /// <summary>
        /// You win, unless you cheated.
        /// The anti cheat won't affect you if you play fair.
        /// By the way, the first anti cheat measure was making
        /// the curQuestion a byte, meaning you would have to
        /// manually change CE settings to scan byte.
        /// </summary>
        /// <param name="r">Type correct, which is an array of correct answers.</param>
        /// <param name="w">Type wrong, which is an array of wrong answers.</param>
        /// <param name="wr">Type deathCount, the normal count of how many deaths.</param>
        public YouWin(int[] r, int[] w, int wr)
        {
            //This boolean will determine the outcome of the game.
            legit = true;

            //If not debugging, the anti cheat will kick in.
            if (!debugMode)
            {
                //This int will calculate your real numbers wrong.
                int wrong = 0;

                //Calculation to see if you really clicked your way to the end.
                for (int i = r.Length - 1; i >= 0; --i)
                {
                    if (r[i] != 7)
                    {
                        legit = false;
                        break;
                    }
                }
                if (legit)
                {
                    //Calculation to see if you really clicked how many you really got wrong.
                    for (int i = w.Length - 1; i >= 0; --i)
                    {
                        wrong += (w[i] / 3);
                    }
                    legit = (wrong == wr);
                }
            }

            if (legit)
            {
                Text = new string[] 
                    {
                    "You have barely survived with:",
                    wr + " fails.",
                    "",
                    "Well, I don't know what to say. You better go to",
                    "Math Lab. That way, you can make up for your lack",
                    "of understanding. Everything you've learned in the",
                    "past is all wrong. Next time, take better notes in",
                    "class.",
                    "",
                    "You have (unfairly) received an:",
                    "F",
                    };

                //Give out ranks.
                switch (wr)
                {
                    case 0:
                        Text = new string[] 
                        {
                        "You have won with:",
                        "No fails!",
                        "",
                        "I congratulate you! You are most certainly qualified",
                        "to be an astronaut for NASA! Go forth and fulfill",
                        "your destiny! If you fail, you can always teach at", 
                        "Stanford, then move on to be a high school math",
                        "teacher...",
                        "",
                        "You passed with an:",
                        "A",
                        };
                        break;
                    case 1:
                        Text = new string[] 
                        {
                        "You have won with:",
                        "One fail.",
                        "",
                        "I congratulate you! You are most certainly qualified",
                        "to be an astronaut for NASA! Go forth and fulfill",
                        "your destiny! If you fail, you can always teach at", 
                        "Stanford, then move on to be a high school math",
                        "teacher...",
                        "",
                        "You passed with an:",
                        "A",
                        };
                        break;
                    case 2:
                    case 3:
                    case 4:
                    case 5:
                    case 6:
                        Text = new string[] 
                        {
                        "You have won with:",
                        wr + " fails.",
                        "",
                        "Darn, it seems you did quite well; there's no need to",
                        "go to math lab... however, based on your score, you",
                        "are very likely going to be a high school math",
                        "teacher. No need to fear; this quiz can be the basis",
                        "for your teachings!",
                        "",
                        "You passed with a:",
                        "B",
                        };
                        break;
                    case 7:
                    case 8:
                    case 9:
                    case 10:
                    case 11:
                    case 12:
                    case 13:
                    case 14:
                        Text = new string[] 
                        {
                        "You have survived with:",
                        wr + " fails.",
                        "",
                        "This score isn't looking quite good. It is average,",
                        "but I recommend going to Math Lab to fix that grade.",
                        "Clearly, you haven't been taking very good notes and", 
                        "you have messy homework, so the blame is on you.",
                        "",
                        "You passed with a:",
                        "C",
                        };
                        break;
                    case 15:
                    case 16:
                    case 17:
                    case 18:
                    case 19:
                    case 20:
                        Text = new string[] 
                        {
                        "You have barely survived with:",
                        wr + " fails.",
                        "",
                        "This grade is your fault. You obviously didn't do",
                        "your homework 'correctly', and you haven't shown",
                        "that you have learned anything. If I were you, I'd", 
                        "go to Math Lab.",
                        "",
                        "You got a:",
                        "D",
                        };
                        break;
                }
            }
            else
            {
                Text = new string[] 
                    {
                    "You thought you could easily cheat?",
                    "Well tough luck! You and your Cheat",
                    "Engine were no match for this quiz!",
                    "Redo the quiz, script kiddie!",
                    "",
                    "You failed, now go home with an F!"
                    };
            }
        }
    }
}

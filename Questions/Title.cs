﻿#region Using Statements
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using ContentLoader;
using Microsoft.Xna.Framework;
using VideoDisplay;
using Microsoft.Xna.Framework.Input;
#endregion

namespace Questions
{
    public class Title : Question
    {
        /// <summary>
        /// Initializes a new title screen. It's just a question in disguised.
        /// </summary>
        /// <param name="q">The game's title screen text.</param>
        /// <param name="a">The game's title screen choices.</param>
        public Title(string[] q, string[] a)
            : base(q, a, -1)
        {
        }

        /// <summary>
        /// If the mouse is within any of the boxes, it will return the
        /// ID of the boxes. Otherwise, it will return -1.
        /// </summary>
        /// <param name="mouseState"></param>
        /// <returns>20 for not clicked. Every other number is the ID.</returns>
        public override byte ClickAnswers(MouseState mouseState)
        {
            if (mouseState.X > 20 && mouseState.X < VideoVariables.WIDTH / 2 - 20)
            {
                if (mouseState.Y > VideoVariables.HEIGHT / 2 + 5 &&
                        mouseState.Y < VideoVariables.HEIGHT * (0.75) - 20)
                {
                    return 0;
                }
                else if (mouseState.Y > VideoVariables.HEIGHT * (.75) + 5 &&
                        mouseState.Y < VideoVariables.HEIGHT - 20)
                {
                    return 1;
                }
            }
            else if (mouseState.X > VideoVariables.WIDTH / 2 + 20 && mouseState.X < VideoVariables.WIDTH - 20)
            {
                if (mouseState.Y > VideoVariables.HEIGHT / 2 + 5 &&
                        mouseState.Y < VideoVariables.HEIGHT * (0.75) - 20)
                {
                    return 2;
                }
                else if (mouseState.Y > VideoVariables.HEIGHT * (.75) + 5 &&
                        mouseState.Y < VideoVariables.HEIGHT - 20)
                {
                    return 3;
                }
            }
            return 20;
        }

        /// <summary>
        /// Displays the game's name on the screen.
        /// </summary>
        public override void DisplayQuestion(SpriteBatch sb)
        {
            Simplify.CenterText(sb,
                TheContentLoader.Font[1],
                QuestionText[0],
                VideoVariables.WIDTH / 2, 50,
                Color.Black);

            for (int i = QuestionText.Length - 1; i >= 1; i--)
            {
                Simplify.CenterText(sb,
                    TheContentLoader.Font[4],
                    QuestionText[i],
                    VideoVariables.WIDTH / 2, 50 + (34 * i),
                    Color.Black);
            }
        }
    }
}

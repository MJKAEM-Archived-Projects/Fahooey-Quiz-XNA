﻿#region Using Statements
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using ContentLoader;
using Microsoft.Xna.Framework;
using VideoDisplay;
#endregion

namespace Questions
{
    public abstract class WallOfText
    {
        private string[] text;

        public void Show(SpriteBatch sb, MouseState mouseState)
        {
            //Display all the text
            for (int i = text.Length - 1; i >= 0; i--)
            {
                Simplify.CenterText(sb,
                    TheContentLoader.Font[0],
                    text[i],
                    VideoVariables.WIDTH / 2, 50 + (30 * i),
                    Color.Black);
            }

            //If mouse over box, highlight the box
            if ((mouseState.X > 20 && mouseState.X < 20 + 300) &&
                (mouseState.Y > VideoVariables.HEIGHT - 20 - 50 && mouseState.Y < VideoVariables.HEIGHT - 20))
            {
                sb.Draw(TheContentLoader.Other[1],
                    new Vector2(20, VideoVariables.HEIGHT - 20 - 50),
                    Color.White);
            }
            else
            {
                sb.Draw(TheContentLoader.Other[0],
                    new Vector2(20, VideoVariables.HEIGHT - 20 - 50),
                    Color.White);
            }
        }

        /// <summary>
        /// Tells game whether or not to goes back to main menu.
        /// </summary>
        /// <param name="mouseState"></param>
        /// <returns>True or false.</returns>
        public bool ClickExit(MouseState mouseState)
        {
            if ((mouseState.X > 20 && mouseState.X < 20 + 300) &&
                (mouseState.Y > VideoVariables.HEIGHT - 20 - 50 && mouseState.Y < VideoVariables.HEIGHT - 20))
            {
                return true;
            }
            return false;
        }

        protected string[] Text { get { return text; } set { text = value; } }
    }
}

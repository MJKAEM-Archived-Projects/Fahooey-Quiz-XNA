#region Using Statements
using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using VideoDisplay;
using ContentLoader;
#endregion

namespace Questions
{
    public class Question
    {
        private string[] questionText;
        private Answer[] answers;

        /// <summary>
        /// It is a question, dummy!
        /// </summary>
        /// <param name="q">The question. Each position in the array moves it down.  Do not use the backslash n.</param>
        /// <param name="a">The array of answers.</param>
        /// <param name="correctNum">Value of 0-3, whichever answer is correct. Any other indicates the boxes are all wrong.</param>
        public Question(string[] q, string[] a, int correctNum)
        {
            questionText = q;
            answers = new Answer[a.Length];
            for (int i = a.Length - 1; i >= 0; --i)
            {
                answers[i] = new Answer(i == correctNum, a[i]);
            }
        }

        /// <summary>
        /// It is a question, dummy!
        /// </summary>
        /// <param name="q">The question. Each position in the array moves it down.  Do not use the backslash n.</param>
        /// <param name="a">The array of answers.</param>
        /// <param name="correctNum">Value of 0-3, whichever answer is correct. Any other indicates the boxes are all wrong.</param>
        public Question(string[] q, string[][] a, int correctNum)
        {
            questionText = q;
            answers = new Answer[a.Length];
            for (int i = a.Length - 1; i >= 0; --i)
            {
                if (a[i].Length == 1)
                {
                    answers[i] = new Answer(i == correctNum, a[i][0]);
                }
                else
                {
                    answers[i] = new Answer(i == correctNum, a[i]);
                }
            }
        }

        /// <summary>
        /// Returns a number indicating whether the buttons are clicked
        /// and whether it is correct.
        /// </summary>
        /// <returns>0 for not clicked, 1 for wrong, 2 for correct.</returns>
        public virtual byte ClickAnswers(MouseState mouseState)
        {
            if (mouseState.X > 20 && mouseState.X < VideoVariables.WIDTH / 2 - 20)
            {
                if (mouseState.Y > VideoVariables.HEIGHT / 2 + 5 &&
                        mouseState.Y < VideoVariables.HEIGHT * (0.75) - 20)
                {
                    if (answers[0].IsCorrect)
                    {
                        return 2;
                    }
                    else
                    {
                        return 1;
                    }
                }
                else if (mouseState.Y > VideoVariables.HEIGHT * (.75) + 5 &&
                        mouseState.Y < VideoVariables.HEIGHT - 20)
                {
                    if (answers[1].IsCorrect)
                    {
                        return 2;
                    }
                    else
                    {
                        return 1;
                    }
                }
            }
            else if (mouseState.X > VideoVariables.WIDTH / 2 + 20 && mouseState.X < VideoVariables.WIDTH - 20)
            {
                if (mouseState.Y > VideoVariables.HEIGHT / 2 + 5 &&
                        mouseState.Y < VideoVariables.HEIGHT * (0.75) - 20)
                {
                    if (answers[2].IsCorrect)
                    {
                        return 2;
                    }
                    else
                    {
                        return 1;
                    }
                }
                else if (mouseState.Y > VideoVariables.HEIGHT * (.75) + 5 &&
                        mouseState.Y < VideoVariables.HEIGHT - 20)
                {
                    if (answers[3].IsCorrect)
                    {
                        return 2;
                    }
                    else
                    {
                        return 1;
                    }
                }
            }
            return 0;
        }

        /// <summary>
        /// Displays the question at the top of the screen.
        /// </summary>
        public virtual void DisplayQuestion(SpriteBatch sb)
        {
            for (int i = questionText.Length - 1; i >= 0; --i)
            {
                Simplify.CenterText(sb,
                    TheContentLoader.Font[1],
                    questionText[i],
                    VideoVariables.WIDTH / 2, 50 + (50 * i),
                    Color.Black);
            }
        }

        public virtual void DisplayAnswers(SpriteBatch sb, MouseState mouseState)
        {
            #region Upper Left Box (0)
            if ((mouseState.X > 20 && mouseState.X < VideoVariables.WIDTH / 2 - 20) &&
                (mouseState.Y > VideoVariables.HEIGHT / 2 + 5 && mouseState.Y < VideoVariables.HEIGHT * (0.75) - 20))
            {
                sb.Draw(TheContentLoader.Normal[1],
                    new Vector2(20, VideoVariables.HEIGHT / 2 + 5),
                    Color.White);
            }
            else
            {
                sb.Draw(TheContentLoader.Normal[0],
                    new Vector2(20, VideoVariables.HEIGHT / 2 + 5),
                    Color.White);
            }
            if (answers[0].AnswerText != null)
            {
                Simplify.CenterText(sb,
                    TheContentLoader.Font[0],
                    answers[0].AnswerText,
                    (float)(VideoVariables.WIDTH *.25), (VideoVariables.HEIGHT / 2 + 5) + 50,
                    Color.Black);
            }
            else
            {
                Simplify.CenterText(sb,
                    TheContentLoader.Font[0],
                    answers[0].AnswerTexts,
                    (float)(VideoVariables.WIDTH * .25), (VideoVariables.HEIGHT / 2 + 5) + 50,
                    Color.Black);
            }
            #endregion
            #region Lower Left Box (1)
            if ((mouseState.X > 20 && mouseState.X < VideoVariables.WIDTH / 2 - 20) &&
                (mouseState.Y > VideoVariables.HEIGHT * (.75) + 5 && mouseState.Y < VideoVariables.HEIGHT - 20))
            {
                sb.Draw(TheContentLoader.Normal[1],
                    new Vector2(20, (float)(VideoVariables.HEIGHT * (.75) + 5)),
                    Color.White);
            }
            else
            {
                sb.Draw(TheContentLoader.Normal[0],
                    new Vector2(20, (float)(VideoVariables.HEIGHT * (.75) + 5)),
                    Color.White);
            }

            if (answers[1].AnswerText != null)
            {
                Simplify.CenterText(sb,
                    TheContentLoader.Font[0],
                    answers[1].AnswerText,
                    (float)(VideoVariables.WIDTH *.25), (float)(VideoVariables.HEIGHT * .75 + 5) + 50,
                    Color.Black);
            }
            else
            {
                Simplify.CenterText(sb,
                    TheContentLoader.Font[0],
                    answers[1].AnswerTexts,
                    (float)(VideoVariables.WIDTH *.25), (float)(VideoVariables.HEIGHT * .75 + 5) + 50,
                    Color.Black);
            }
            #endregion
            #region Upper Right Box (2)
            if ((mouseState.X > VideoVariables.WIDTH / 2 + 20 && mouseState.X < VideoVariables.WIDTH - 20) &&
                (mouseState.Y > VideoVariables.HEIGHT / 2 + 5 && mouseState.Y < VideoVariables.HEIGHT * (0.75) - 20))
            {
                sb.Draw(TheContentLoader.Normal[1],
                    new Vector2(VideoVariables.WIDTH / 2 + 20, VideoVariables.HEIGHT / 2 + 5),
                    Color.White);
            }
            else
            {
                sb.Draw(TheContentLoader.Normal[0],
                    new Vector2(VideoVariables.WIDTH / 2 + 20, VideoVariables.HEIGHT / 2 + 5),
                    Color.White);
            }
            if (answers[2].AnswerText != null)
            {
                Simplify.CenterText(sb,
                    TheContentLoader.Font[0],
                    answers[2].AnswerText,
                    (float)(VideoVariables.WIDTH * .75), (VideoVariables.HEIGHT / 2 + 5) + 50,
                    Color.Black);
            }
            else
            {
                Simplify.CenterText(sb,
                    TheContentLoader.Font[0],
                    answers[2].AnswerTexts,
                    (float)(VideoVariables.WIDTH * .75), (VideoVariables.HEIGHT / 2 + 5) + 50,
                    Color.Black);
            }
            #endregion
            #region Lower Right Box (3)
            if ((mouseState.X > VideoVariables.WIDTH / 2 + 20 && mouseState.X < VideoVariables.WIDTH - 20) &&
                (mouseState.Y > VideoVariables.HEIGHT * (.75) + 5 && mouseState.Y < VideoVariables.HEIGHT - 20))
            {
                sb.Draw(TheContentLoader.Normal[1],
                    new Vector2(VideoVariables.WIDTH / 2 + 20, (float)(VideoVariables.HEIGHT * (.75) + 5)),
                    Color.White);
            }
            else
            {
                sb.Draw(TheContentLoader.Normal[0],
                    new Vector2(VideoVariables.WIDTH / 2 + 20, (float)(VideoVariables.HEIGHT * (.75) + 5)),
                    Color.White);
            }
            if (answers[3].AnswerText != null)
            {
                Simplify.CenterText(sb,
                    TheContentLoader.Font[0],
                    answers[3].AnswerText,
                    (float)(VideoVariables.WIDTH * .75), (float)(VideoVariables.HEIGHT * .75 + 5) + 50,
                    Color.Black);
            }
            else
            {
                Simplify.CenterText(sb,
                    TheContentLoader.Font[0],
                    answers[3].AnswerTexts,
                    (float)(VideoVariables.WIDTH * .75), (float)(VideoVariables.HEIGHT * .75 + 5) + 50,
                    Color.Black);
            }
            #endregion
        }

        public int GetCorrectAnswer()
        {
            for (int i = answers.Length - 1; i >= 0; --i)
            {
                if (answers[i].IsCorrect)
                {
                    return i;
                }
            }
            return -1;
        }
        public void ChangeAnswers(string[] str) { for (int i = answers.Length - 1; i >= 0; i--) { answers[i].AnswerText = str[i]; } }
        public Answer[] Answers { get { return answers; } }
        public void SetQuestionText(int index, string str) { questionText[index] = str; }
        public string[] QuestionText { get { return questionText; } set { questionText = value; } }
    }
}

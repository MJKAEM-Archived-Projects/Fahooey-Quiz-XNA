﻿#region Using Statements
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;
using ErrorDepartment;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Media;
#endregion

namespace ContentLoader
{
    public static class TheContentLoader
    {
        public static bool trueTheme;

        public static SpriteFont[] Font = new SpriteFont[6];
        public static Texture2D[] Normal = new Texture2D[12];
        public static Texture2D[] SixSixSix = new Texture2D[12];
        public static Texture2D[] Other = new Texture2D[5];
        public static SoundEffect[] Sound = new SoundEffect[5];
        public static Song[] Music = new Song[2];

        public static void LoadEverything(ContentManager cm)
        {
            Console.WriteLine("Let's load everything!\n");

            #region Load Font
            ConsolePrinter.BluePrint("Loading " + Font.Length + " fonts.");
            try { LoadFont(cm); }
            catch (ContentLoadException e) { ConsolePrinter.RedPrint("Missing a font? No playing for you.\n"); throw e; }
            ConsolePrinter.GreenPrint("Loading succeeded!\n");
            #endregion

            #region Load Normal
            try
            {
                ConsolePrinter.BluePrint("Loading " + Normal.Length + " buttons.");
                LoadNormalContent(cm);
                ConsolePrinter.GreenPrint("Loading succeeded!\n");
            }
            catch (ContentLoadException e)
            {
                ConsolePrinter.RedPrint("Loading failed!\n");
                ConsolePrinter.GrayPrint(e + "\n");
                ConsolePrinter.RedPrint("This means you are missing a file. Please redownload the game.");
                //ConsolePrinter.YellowPrint("However, you may continue playing, but with messed up textures.\n");
                //if (HandleItAll.ResponseToFailure())
                //{
                throw e;
                //}
            }
            #endregion

            #region Load 666
            try
            {
                ConsolePrinter.BluePrint("Loading " + SixSixSix.Length + " souls.");
                Load666Content(cm);
                ConsolePrinter.GreenPrint("Loading succeeded!\n");
            }
            catch (ContentLoadException e)
            {
                ConsolePrinter.RedPrint("Loading failed!\n");
                ConsolePrinter.GrayPrint(e + "\n");
                ConsolePrinter.RedPrint("This means you are missing a file. Please redownload the game.");
                //ConsolePrinter.YellowPrint("However, you may continue playing, but with messed up textures.\n");
                //if (HandleItAll.ResponseToFailure())
                //{
                throw e;
                //}
            }
            #endregion

            #region Load Other
            try
            {
                ConsolePrinter.BluePrint("Loading " + Other.Length + " other files.");
                LoadOtherContent(cm);
                ConsolePrinter.GreenPrint("Loading succeeded!\n");
            }
            catch (ContentLoadException e)
            {
                ConsolePrinter.RedPrint("Loading failed!\n");
                ConsolePrinter.GrayPrint(e + "\n");
                ConsolePrinter.RedPrint("This means you are missing a file. Please redownload the game.");
                //ConsolePrinter.YellowPrint("However, you may continue playing, but with messed up textures.\n");
                //if (HandleItAll.ResponseToFailure())
                //{
                throw e;
                //}
            }
            #endregion

            #region Load Sounds
            try
            {
                ConsolePrinter.BluePrint("Loading " + Sound.Length + " sound files.");
                LoadSoundContent(cm);
                ConsolePrinter.GreenPrint("Loading succeeded!\n");
            }
            catch (ContentLoadException e)
            {
                ConsolePrinter.RedPrint("Loading failed!\n");
                ConsolePrinter.GrayPrint(e + "\n");
                ConsolePrinter.RedPrint("This means you are missing a file. Please redownload the game.");
                ConsolePrinter.YellowPrint("However, you may continue playing, but with no sound.\n");
                if (HandleItAll.ResponseToFailure())
                {
                    throw e;
                }
                for (int i = Sound.Length - 1; i >= 0; --i)
                {
                    Sound[i] = null;
                }
            }
            #endregion

            #region Load Music
            try
            {
                ConsolePrinter.BluePrint("Loading " + Music.Length + " music files.");
                LoadMusicContent(cm);
                ConsolePrinter.GreenPrint("Loading succeeded!\n");
            }
            catch (ContentLoadException e)
            {
                ConsolePrinter.RedPrint("Loading failed!\n");
                ConsolePrinter.GrayPrint(e + "\n");
                ConsolePrinter.RedPrint("This means you are missing a file. Please redownload the game.");
                ConsolePrinter.YellowPrint("However, you may continue playing, but with no sounds.\n");
                if (HandleItAll.ResponseToFailure())
                {
                    throw e;
                }
                for (int i = Music.Length - 1; i >= 0; --i)
                {
                    Music[i] = null;
                }
            }
            #endregion
        }
        private static void LoadFont(ContentManager cm)
        {
            Font[0] = cm.Load<SpriteFont>("Font/Regular18");
            Font[1] = cm.Load<SpriteFont>("Font/Regular35");
            Font[2] = cm.Load<SpriteFont>("Font/Chiller18");
            Font[3] = cm.Load<SpriteFont>("Font/Chiller35");
            Font[4] = cm.Load<SpriteFont>("Font/Regular21");
            Font[5] = cm.Load<SpriteFont>("Font/UnicodeFixer35");
        }
        private static void LoadNormalContent(ContentManager cm)
        {
            Normal[0] = cm.Load<Texture2D>("Image/Normal/Box");
            Normal[1] = cm.Load<Texture2D>("Image/Normal/HBox");
            #region Sound Buttons
            Normal[2] = cm.Load<Texture2D>("Image/Normal/SoundBox");
            Normal[3] = cm.Load<Texture2D>("Image/Normal/HSoundBox");
            Normal[4] = cm.Load<Texture2D>("Image/Normal/SoundBoxX");
            Normal[5] = cm.Load<Texture2D>("Image/Normal/HSoundBoxX");
            #endregion
            #region Music Buttons
            Normal[6] = cm.Load<Texture2D>("Image/Normal/MusicBox");
            Normal[7] = cm.Load<Texture2D>("Image/Normal/HMusicBox");
            Normal[8] = cm.Load<Texture2D>("Image/Normal/MusicBoxX");
            Normal[9] = cm.Load<Texture2D>("Image/Normal/HMusicBoxX");
            #endregion
            Normal[10] = cm.Load<Texture2D>("Image/Normal/BigBox");
            Normal[11] = cm.Load<Texture2D>("Image/Normal/BigHBox");
        }
        private static void Load666Content(ContentManager cm)
        {
            SixSixSix[0] = cm.Load<Texture2D>("Image/SixSixSix/Box666");
            SixSixSix[1] = cm.Load<Texture2D>("Image/SixSixSix/HBox666");
            #region Sound Buttons
            SixSixSix[2] = cm.Load<Texture2D>("Image/SixSixSix/SoundBox666");
            SixSixSix[3] = cm.Load<Texture2D>("Image/SixSixSix/HSoundBox666");
            SixSixSix[4] = cm.Load<Texture2D>("Image/SixSixSix/SoundBoxX666");
            SixSixSix[5] = cm.Load<Texture2D>("Image/SixSixSix/HSoundBoxX666");
            #endregion
            #region Music Buttons
            SixSixSix[6] = cm.Load<Texture2D>("Image/SixSixSix/MusicBox666");
            SixSixSix[7] = cm.Load<Texture2D>("Image/SixSixSix/HMusicBox666");
            SixSixSix[8] = cm.Load<Texture2D>("Image/SixSixSix/MusicBoxX666");
            SixSixSix[9] = cm.Load<Texture2D>("Image/SixSixSix/HMusicBoxX666");
            #endregion
            SixSixSix[10] = cm.Load<Texture2D>("Image/SixSixSix/BigBox666");
            SixSixSix[11] = cm.Load<Texture2D>("Image/SixSixSix/BigHBox666");
        }
        private static void LoadOtherContent(ContentManager cm)
        {
            Other[0] = cm.Load<Texture2D>("Image/Back to main menu");
            Other[1] = cm.Load<Texture2D>("Image/HBack to main menu");
            Other[2] = cm.Load<Texture2D>("Image/Sine");
            Other[3] = cm.Load<Texture2D>("Image/Triangle");
            Other[4] = cm.Load<Texture2D>("Image/Star");
        }
        private static void LoadSoundContent(ContentManager cm)
        {
            Sound[0] = cm.Load<SoundEffect>("Sound/DING");
            Sound[1] = cm.Load<SoundEffect>("Sound/Bike Horn");
            Sound[2] = cm.Load<SoundEffect>("Sound/Sigh");
            Sound[3] = cm.Load<SoundEffect>("Sound/Train_Honk_Horn_Clear");
            Sound[4] = cm.Load<SoundEffect>("Sound/nuclear siren");
        }
        private static void LoadMusicContent(ContentManager cm)
        {
            if (trueTheme)
            {
                Music[0] = cm.Load<Song>("Music/song000");
            }
            else
            {
                Music[0] = cm.Load<Song>("Music/song001");
            }
            Music[1] = cm.Load<Song>("Music/song002");
        }

        public static bool TrueTheme { get { return trueTheme; } set { trueTheme = value; } }
    }
}

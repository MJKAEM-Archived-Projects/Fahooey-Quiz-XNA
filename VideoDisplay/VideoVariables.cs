#region Using Statements
using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
#endregion

namespace VideoDisplay
{
    public static class VideoVariables
    {
        public const int WIDTH = 640, HEIGHT = 480;
        private static bool fullscreen;

        public static bool Fullscreen { get { return fullscreen; } set { fullscreen = value; } }
    }
}

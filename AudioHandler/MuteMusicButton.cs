﻿#region Using Statements
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using VideoDisplay;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using ContentLoader;
using Microsoft.Xna.Framework;
#endregion

namespace AudioHandler
{
    public class MuteMusicButton : AbstractMuteButton
    {
        public MuteMusicButton()
            : base(VideoVariables.WIDTH - 110, VideoVariables.HEIGHT / 2 - 60)
        {
        }
        public override void Display(SpriteBatch sb, MouseState mouseState)
        {
            //Draw box
            if ((mouseState.X > PosX && mouseState.X < PosX + 35) &&
                (mouseState.Y > PosY && mouseState.Y < PosY + 35))
            {
                if (Muted == 1 || Muted == 2)
                {
                    sb.Draw(TheContentLoader.Normal[9],
                        new Vector2(PosX, PosY),
                        Color.White);
                }
                else
                {
                    sb.Draw(TheContentLoader.Normal[7],
                        new Vector2(PosX, PosY),
                        Color.White);
                }
            }
            else
            {
                if (Muted == 1 || Muted == 2)
                {
                    sb.Draw(TheContentLoader.Normal[8],
                        new Vector2(PosX, PosY),
                        Color.White);
                }
                else
                {
                    sb.Draw(TheContentLoader.Normal[6],
                        new Vector2(PosX, PosY),
                        Color.White);
                }
            }
        }
        public override void Display666(SpriteBatch sb, MouseState mouseState)
        {
            //Draw box
            if ((mouseState.X > PosX && mouseState.X < PosX + 35) &&
                (mouseState.Y > PosY && mouseState.Y < PosY + 35))
            {
                if (Muted == 1 || Muted == 2)
                {
                    sb.Draw(TheContentLoader.SixSixSix[9],
                        new Vector2(PosX, PosY),
                        Color.White);
                }
                else
                {
                    sb.Draw(TheContentLoader.SixSixSix[7],
                        new Vector2(PosX, PosY),
                        Color.White);
                }
            }
            else
            {
                if (Muted == 1 || Muted == 2)
                {
                    sb.Draw(TheContentLoader.SixSixSix[8],
                        new Vector2(PosX, PosY),
                        Color.White);
                }
                else
                {
                    sb.Draw(TheContentLoader.SixSixSix[6],
                        new Vector2(PosX, PosY),
                        Color.White);
                }
            }
        }
    }
}

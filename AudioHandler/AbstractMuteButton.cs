﻿#region Using Statements
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using VideoDisplay;
using ContentLoader;
#endregion

namespace AudioHandler
{
    public abstract class AbstractMuteButton
    {
        private float posX, posY;
        private byte muted;

        protected AbstractMuteButton(float x, float y)
        {
            posX = x;
            posY = y;
        }

        /// <summary>
        /// Displays the mute button for sound.
        /// </summary>
        public abstract void Display(SpriteBatch sb, MouseState mouseState);

        /// <summary>
        /// ...I'm..S..o...R...ry
        /// </summary>
        public abstract void Display666(SpriteBatch sb, MouseState mouseState);

        /// <summary>
        /// If box is clicked, set to false.
        /// </summary>
        /// <returns></returns>
        public bool Clicked(MouseState mouseState)
        {
            if ((mouseState.X > posX && mouseState.X < posX + 35) &&
                (mouseState.Y > posY && mouseState.Y < posY + 35))
            {
                switch (muted)
                {
                    case 0:
                        muted = 1;
                        break;
                    case 1:
                        muted = 0;
                        break;
                }
                return true;
            }
            return false;
        }

        public byte Muted { get { return muted; } set { muted = value; } }
        protected float PosX { get { return posX; } set { posX = value; } }
        protected float PosY { get { return posY; } set { posY = value; } }
    }
}

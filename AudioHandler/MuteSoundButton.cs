﻿#region Using Statements
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ContentLoader;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using VideoDisplay;
#endregion

namespace AudioHandler
{
    public class MuteSoundButton : AbstractMuteButton
    {
        public MuteSoundButton()
            : base(VideoVariables.WIDTH - 60, VideoVariables.HEIGHT / 2 - 60)
        {
        }
        public override void Display(SpriteBatch sb, MouseState mouseState)
        {
            //Draw box
            if ((mouseState.X > PosX && mouseState.X < PosX + 35) &&
                (mouseState.Y > PosY && mouseState.Y < PosY + 35))
            {
                if (Muted == 1 || Muted == 2)
                {
                    sb.Draw(TheContentLoader.Normal[5],
                        new Vector2(PosX, PosY),
                        Color.White);
                }
                else
                {
                    sb.Draw(TheContentLoader.Normal[3],
                        new Vector2(PosX, PosY),
                        Color.White);
                }
            }
            else
            {
                if (Muted == 1 || Muted == 2)
                {
                    sb.Draw(TheContentLoader.Normal[4],
                        new Vector2(PosX, PosY),
                        Color.White);
                }
                else
                {
                    sb.Draw(TheContentLoader.Normal[2],
                        new Vector2(PosX, PosY),
                        Color.White);
                }
            }
        }
        public override void Display666(SpriteBatch sb, MouseState mouseState)
        {
            //Draw box
            if ((mouseState.X > PosX && mouseState.X < PosX + 35) &&
                (mouseState.Y > PosY && mouseState.Y < PosY + 35))
            {
                if (Muted == 1 || Muted == 2)
                {
                    sb.Draw(TheContentLoader.SixSixSix[5],
                        new Vector2(PosX, PosY),
                        Color.White);
                }
                else
                {
                    sb.Draw(TheContentLoader.SixSixSix[3],
                        new Vector2(PosX, PosY),
                        Color.White);
                }
            }
            else
            {
                if (Muted == 1 || Muted == 2)
                {
                    sb.Draw(TheContentLoader.SixSixSix[4],
                        new Vector2(PosX, PosY),
                        Color.White);
                }
                else
                {
                    sb.Draw(TheContentLoader.SixSixSix[2],
                        new Vector2(PosX, PosY),
                        Color.White);
                }
            }
        }
    }
}

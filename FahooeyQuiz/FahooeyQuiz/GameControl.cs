﻿#region Using Statements
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;
using Questions;
using ContentLoader;
using VideoDisplay;
using Microsoft.Xna.Framework.Input;
using AudioHandler;
using Questions.SpecialQuestions;
using Microsoft.Xna.Framework.Media;
using Microsoft.Xna.Framework.Audio;
using ErrorDepartment;
#endregion

namespace FahooeyQuiz
{
    public class GameControl
    {
        public const int QUESTION_NUM = 55, TURNOVER_NUM = 42;
        public const bool debugMode = true;

        public static bool trueTheme, keyWinBlade;
        //public static bool final10;

        private SoundEffectInstance correctSound, wrongSound, final10Sound, startSound, final10Wrong;

        private bool showFR;
        private byte gameState;
        private byte curQuestion;
        private int deathCount;

        private Question[] questions = new Question[QUESTION_NUM];
        private int[] correct = new int[QUESTION_NUM];
        private int[] wrong = new int[QUESTION_NUM];
        private Title title;
        private Instructions instructions;
        private Credits credits;
        private YouWin youWin;

        private MuteSoundButton muteSoundButton;
        private MuteMusicButton muteMusicButton;

        public void Setup()
        {
            MediaPlayer.IsRepeating = true;

            instructions = new Instructions();
            credits = new Credits(trueTheme);
            muteSoundButton = new MuteSoundButton();
            muteMusicButton = new MuteMusicButton();
            
            title = new Title(
                new string[]
                {
                    "Fahooey Quiz",
                    "by a Frustrated Lunatic",
                    "Pre-Calculus 2013"
                },
                new string[] { "Start train", "Credits", "Instructions", "Exit" }
                );

            #region Questions 0 - 9
            questions[0] = new Question(
                new string[]
                {
                    "Which is the next correct",
                    "step in getting 0",
                    "from sin(180\u00B0)?"
                },
                new string[] { "sin(180\u00B0)", "0", "sin(\u03C0 + 2\u03C0)", "sin(\u03C0)" },
                2);

            questions[1] = new Question(
                new string[]
                {
                    "What is the most",
                    "important part",
                    "when graphing?"
                },
                new string[] { "Scaling", "Plotting points", "Using a ruler", "All of the choices" },
                0);

            questions[2] = new Question(
                new string[]
                {
                    "Correctly address the next",
                    "step: x\u00B2 - 25 = 0"
                },
                new string[] { "x = \u00B1\u221A(25)", "x\u00B2 = 25", "(x - 5)(x + 5) = 0", "x = \u00B15" },
                2);

            questions[3] = new Question(
                new string[]
                {
                    "Print me a picture of"
                },
                new string[] { "Neil Armstrong", "Janet Voss", "Issac Newton", "Alan B. Shepard" },
                1);

            questions[4] = new Question(
                    new string[]
                        {
                "Your circle isn't very"
                        },
                            new string[] { "Radial", "Scaled", "Round", "Circular" },
                            3);

            questions[5] = new Question(
                    new string[]
                        {
                "Your radius isn't very"
                        },
                            new string[] { "Scaled", "Radial", "Long", "Short" },
                            1);

            questions[6] = new Question(
                    new string[]
                        {
                "Your angle isn't very"
                        },
                            new string[] { "Angular", "Archival", "Anglican", "Archaic" },
                            0);

            questions[7] = new Question(
                    new string[]
                        {
                "In English, what is the",
                "next step for 12\u00F72?"
                        },
                            new string[] { "Divide by 2", "Multiply by 1/2", "Split it in half", "Divvy it up" },
                            3);

            questions[8] = new Question(
                    new string[]
                        {
                "When plugging in numbers",
                "into equations, we are",
                "trying to"
                        },
                            new string[] { "See if it shakes", "Shake it up", "See what shakes", "Be an astronaut" },
                            2);

            questions[9] = new Question(
                    new string[]
                        {
                "Not paying attention?",
                "Time for"
                        },
                            new string[] { "Ritalin spray", "'Suspension' jokes", "Captivation", "Sister Mary Goliath" },
                            0);
            #endregion
            #region Questions 10 - 19
            questions[10] = new Question(
                new string[]
                        {
                "Name the first step",
                "when solving",
                "[f(x+h)-f(x)]/h"
                        },
                        new string[][]
                        { 
                        new string[] {"Factor"}, 
                        new string[] {"Plug numbers into h,", "while x = 1"},
                        new string[] {"Divvy it up"},
                        new string[] {"Write the equation", "down"}
                        },
                        3);

            questions[11] = new Question(
                    new string[]
                        {
                "I am an expert at",
                "playing the"
                        },
                            new string[] { "Banjo", "Guitar", "Mandolin", "Fiddle" },
                            2);

            questions[12] = new Question12(
                    new string[]
                        {
                "\u00BFHabla espa\u00F1ol?"
                        },
                            new string[] { "Io non parlo spagnolo.", "No hablo ingl\u00E9s.", "Je ne parle pas anglais.", "In English, please." },
                            0);

            questions[13] = new Question(
                    new string[]
                        {
                "I see you have done",
                "your homework!"
                        },
                            new string[] { "Upside down cross", "Anti religion", "Devil horns", "Pentagram of evil" },
                            3);

            questions[14] = new Question(
                    new string[]
                        {
                "Who loves scaled",
                "graphs the most?"
                        },
                            new string[] { "Sister Mary Goliath", "Fahooey", "Fahooey's students", "Satan" },
                            0);

            questions[15] = new Question(
                    new string[]
                        {
                "Hey!!!"
                        },
                            new string[] { "Stop wasting time!", "Come back to me!", "Be here now!", "No time to wander!" },
                            2);

            questions[16] = new Question(
                    new string[]
                        {
                "I am too lazy to teach,",
                "So go watch"
                        },
                            new string[] { "Khan Academy", "PatrickJNT", "Khan Acadamy", "PatrickJMT" },
                            3);

            questions[17] = new Question17(
                    new string[]
                        {
                "Correctly obtain the cos(0)."
                        },
                            new string[] { "0", "-0", "-1", "Undefined" }
                    );

            questions[18] = new Question18(
                    new string[]
                        {
                "Now, get the secant."
                        },
                            new string[] { "1/cos(1)", "1", "1/cos(0)", "I forgot the angle." },
                            2);

            questions[19] = new Question(
                    new string[]
                        {
                "Doing more than needed",
                "earns you how many",
                "points out of 12?"
                        },
                            new string[] { "12", "16", "6", "0" },
                            3);
            #endregion
            #region Questions 20 - 29
            questions[20] = new Question(
                    new string[]
                        {
                "How likely is",
                "partial credit?"
                        },
                            new string[] { "Go to math lab.", "Almost never.", "20% chance.", "Never." },
                            0);

            questions[21] = new Question(
                    new string[]
                        {
                "How likely is",
                "a curve?"
                        },
                            new string[] { "Go to math lab.", "Almost never.", "20% chance.", "Never." },
                            0);

            questions[22] = new Question(
                    new string[]
                        {
                "What happens in",
                "Math Lab?"
                        },
                            new string[] { "Good grades.", "Conversations.", "Happiness.", "A Great Depression." },
                            3);

            questions[23] = new Question(
                    new string[]
                        {
                "\u03C9 stands for"
                        },
                            new string[] { "Ohms", "Angular speed", "\u03A9", "w" },
                            1);

            questions[24] = new Question24(
                    new string[]
                        {
                            "\u223F is"
                        },
                            new string[] { "sin(x)", "A sine graph", "Not quite sinual", "Not scaled properly" },
                            3);

            questions[25] = new Question25(
                    new string[] { "Perfect graph", "Sine graph", "A very sinual graph", "Cosine graph" },
                    2);

            questions[26] = new Question(
                    new string[]
                        {
                "Test postponed guys!",
                "The test will be on"
                        },
                            new string[] { "still the same day", "one day after", "the next week", "the next Monday" },
                            0);

            questions[27] = new Question(
                    new string[]
                        {
                "Pick the scenario",
                "that makes the most",
                "sense"
                        },
                            new string[] { "Long test; long day", "Long test; short day", "Short test; long day", "Short test; short day" },
                            1);

            questions[28] = new Question28(
                new string[]
                        {
                "Sum Difference Formula",
                "Jingle For sin(\u03B1 + \u03B2)!",
                ""
                        },
                        new string[] { "\u03B1", "sine", "cosine", "\u03B2" }
                );

            questions[29] = new Question(
                    new string[]
                        {
                "How many posts on",
                "School Loop are needed",
                "to get people's attention?"
                        },
                            new string[] { "1", "3", "2", "4" },
                            1);
            #endregion
            #region Questions 30 - 39
            questions[30] = new Question(
                    new string[]
                        {
                "The radius is 20 meters,",
                "1 rev. takes 2 secs.",
                "What is missing?"
                        },
                            new string[] { "Critical points", "Amplitude", "Dependent variable(s)", "Independent variable(s)" },
                            0);

            questions[31] = new Question(
                    new string[]
                        {
                "How much time do we",
                "have left in class?"
                        },
                            new string[] { "5 minutes", "More than 30 minutes", "About 15 minutes", "Less than 10 minutes" },
                            3);

            questions[32] = new Question(
                    new string[]
                        {
                "Choose the best advice",
                "when doing word",
                "problems."
                        },
                            new string[] { "Draw pictures.", "Domains: [0, \u03C0]", "Check your work.", "Write a summary." },
                            3);

            questions[33] = new Question(
                    new string[]
                        {
                "The calculator should"
                        },
                            new string[] { "check your work.", "not be trusted.", "not be blindly trusted.", "be used as doorstops." },
                            2);

            questions[34] = new Question(
                    new string[]
                        {
                "Fall 2013: MRI count is"
                        },
                            new string[] { "0", "2", "1", "3" },
                            1);

            questions[35] = new Question34(
                    new string[]
						{
				"What configuration",
				"and what law?"
						},
                            new string[] { "SAS, Cosine", "SAS, Sine.", "ASS, Sine", "SSA, Cosine" },
                            2);

            questions[36] = new Question(
                    new string[]
						{
				"A boy fails a test",
				"with 35 questions and",
				"doesn't go to math lab.",
				"Why's this fake?"
						},
                            new string[][] 
                            { 
                                new string[] {"He didn't go", "to math lab."}, 
                                new string[] {"There are", "35 questions."}, 
                                new string[] {"No one fails a test."}, 
                                new string[] {"It's not fake; it's true."} 
                            },
                            1);

            questions[37] = new Question(
                    new string[]
						{
				"PatrickJMT is not only",
				"cool, but he is:"
						},
                            new string[] { "Amazing", "Cute", "Tough", "Smart" },
                            1);

            questions[38] = new Question38(
                    new string[]
						{
				"Money goes into a bank",
				"account and you gain",
				"money. What's this called?",
				""
						},
                            new string[] { "Amortization", "Anonymity", "Greed", "Astrology" }
                    );

            questions[39] = new Question(
                    new string[]
						{
				"No one's listening",
				"again! You know what",
				"that means!"
						},
                            new string[] { "Sister Mary Goliath!", "Mandolin time!", "Suction cup shoes!", "Ritalin spray!" },
                            2);
            #endregion
            #region Questions 40 - 49
            questions[40] = new Question(
                new string[]
						{
				"What is not a",
				"factor in tests?"
						},
                        new string[] { "Time", "a\u00B2-b\u00B2", "NASA space centers", "Money" },
                        0);

            questions[41] = new Question(
                    new string[]
						{
				"What is the next step for",
                "induction after writing",
                "down the equation?"
						},
                            new string[][] 
                            { 
                                new string[] {"Basis Statement"},
                                new string[] {"Let n and k be", "natural numbers"},
                                new string[] {"Deduction Statement"},
                                new string[] {"Induction Statement"}
                            },
                            1);

            questions[TURNOVER_NUM] = new QuestionTurnover(
                    new string[]
						{
				"People in the hallway",
				"are too loud!"
						},
                            new string[] { "Shut the door..." }
                    );

            questions[43] = new Question666(
                    new string[]
						{
				"F...U...N...K...Y...J...O...H...N"
						},
                            new string[] { "Tesser to the moon", "The great mathematician!", "Drowning in space", "Saloon, halfway to the moon!" },
                            3);

            questions[44] = new Question666(
                    new string[]
						{
				"Angle of depression..."
						},
                            new string[][]
                            { 
                                new string[] {"is proportional to your suffering"},
                                new string[] {"does not exist"},
                                new string[] {"is the angle of elevation"},
                                new string[] {"is always related to two cars","below a cliff"}
                            },
                            2);

            questions[45] = new Question666(
                    new string[]
						{
				"I play ??? for ???",
						},
                            new string[] { "Rock n Roll for elderly", "Jazz for bums", "Rock for hippies", "Clown for students" },
                            1);

            questions[46] = new Question45(
                    new string[]
						{
				"Did you get him...",
						},
                            new string[] { "Y...E...S...", "No!", "I...filed...incomplete...", "H...E...L...P..." }
                    );

            questions[47] = new Question666(
                    new string[]
						{
				"I want to give you a star...",
				"I like your homework..."
						},
                            new string[] { "0 points", "10 points", "20 points", "25 points" },
                            0);

            questions[48] = new Question666(
                    new string[]
						{
				"Name the greatest mathmetician of",
				"the 21st century."
						},
                            new string[] { "Issac Newton", "Janet Voss", "Blaise Pascal", "Grigori Perelman" },
                            3);

            questions[49] = new Question666(
                    new string[]
						{
				"REALITY CHECK:",
				"Are you brainwashed?"
						},
                            new string[] { "No! Fahooey is the best!", "No way! I'm feel awesome!", "I..I..don't know...", "SAVE ME" },
                            3);
            #endregion
            #region Questions 50 - 54
            questions[50] = new Question666(
                    new string[]
						{
				"As you stay in class...",
				"your lifespan shrinks..."
						},
                            new string[] { "Like a tangent function.", "Like a mouse.", "Infinitesimally.", "Very little. I'm already in HELL." },
                            2);

            questions[51] = new Question666(
                    new string[]
						{
				"Guess what happened to the mouse...",
						},
                            new string[] { "It was sacrficed...", "It ran away...it survived...", "...M...o...UsE...Y...I'm..S..o...R...ry", "It was eaten alive..." },
                            1);

            questions[52] = new Question666(
                    new string[]
						{
				"HEY KIDS, WANNA SEE",
                "SISTER MARY GOLIATH?",
						},
                            new string[] { "SHE ISN'T REAL", "PLEASE NO", "YES YES YES YES YES YES YES", "GET ME OUT OF HERE" },
                            0);

            questions[53] = new Question666(
                    new string[]
						{
				"Can you feel the SUNSHINE?",
                "DOES IT BRIGHTEN YOUR DAY?"
						},
                            new string[] { "JOHN...JOHN...JOHN...JOHN...", "STOP PLAYING THIS GAME", "Please close the windows...it is COLD", "i'M...D...y...I...nG" },
                            2);

            questions[QUESTION_NUM - 1] = new QuestionFinal(
                    new string[]
						{
				"I ain't your Antichrist,",
						},
                            new string[] { "I'm your Uncle Sam!" }
                    );

            #endregion

            #region Answer count
            int[] jack = { 0, 0, 0, 0 };

            for (int i = 0; i < QUESTION_NUM && questions[i] != null; ++i)
            {
                int lol = questions[i].GetCorrectAnswer();

                if (lol != -1)
                {
                    ++jack[lol];
                }
            }

            ConsolePrinter.BluePrint("Printing answer count for normal questions...");
            ConsolePrinter.GrayPrint("There are " + QUESTION_NUM + " questions.");
            Console.Write('[');
            Console.Write(jack[0] + ",");
            Console.Write(jack[1] + ",");
            Console.Write(jack[2] + ",");
            Console.Write(jack[3]);
            Console.WriteLine(']');
            ConsolePrinter.GreenPrint("That looks correct!\n");
            #endregion

            //gameState = 1;
            //curQuestion = 51;

            #region Placeholder Questions to Prevent NullReference
            for (int i = QUESTION_NUM - 1; i >= 0 && questions[i] == null; --i)
            {
                if (i < 40)
                {
                    questions[i] = new Question(
                            new string[] { "Insert question." },
                            new string[] { "Answer 0", "Answer 1", "Answer 2", "Answer 3" },
                            1);
                }
                else
                {
                    questions[i] = new Question666(
                            new string[] { "H...E...L...P...M...E..." },
                            new string[] { "Forgive me Lord, for I have sinned", "666", "13", "444" },
                            0);
                }
            }
            #endregion
        }
        public void SetInstanceSounds()
        {
            correctSound = TheContentLoader.Sound[0].CreateInstance();
            wrongSound = TheContentLoader.Sound[1].CreateInstance();
            final10Wrong = TheContentLoader.Sound[2].CreateInstance();
            startSound = TheContentLoader.Sound[3].CreateInstance();
            final10Sound = TheContentLoader.Sound[4].CreateInstance();
        }
        public void Draw(GameTime gameTime, GraphicsDevice gd, SpriteBatch sb, MouseState mouseState)
        {
            if (!Final10)
            {
                gd.Clear(Color.White); //background(255);
            }
            else
            {
                gd.Clear(Color.Black); //background(0);
            }

            #region Display FPS
            if (showFR)
            {
                float frameRate = 1 / (float)gameTime.ElapsedGameTime.TotalSeconds;
                if (Final10)
                {
                    Simplify.LeftText(sb,
                        TheContentLoader.Font[2],
                        ((int)frameRate).ToString(),
                        VideoVariables.WIDTH - 50, VideoVariables.HEIGHT / 2,
                        Color.White);
                }
                else
                {
                    Simplify.LeftText(sb,
                        TheContentLoader.Font[0],
                        ((int)frameRate).ToString(),
                        VideoVariables.WIDTH - 50, VideoVariables.HEIGHT / 2,
                        Color.Black);
                }
            }
            #endregion

            switch (gameState)
            {
                #region Title Screen
                case 0:
                    title.DisplayQuestion(sb);
                    title.DisplayAnswers(sb, mouseState);
                    muteSoundButton.Display(sb, mouseState);
                    muteMusicButton.Display(sb, mouseState);
                    break;
                #endregion

                #region Quiz
                case 1:
                    if (!Final10)
                    {
                        if (curQuestion != 17)
                        {
                            Simplify.LeftText(sb,
                                TheContentLoader.Font[0],
                                "Level: " + (curQuestion + 1),
                                20, VideoVariables.HEIGHT / 2 - 45/*72*/,
                                Color.Black);
                        }
                        else
                        {
                            #region Question 17
                            Simplify.LeftText(sb,
                                TheContentLoader.Font[0],
                                "Level:",
                                20, VideoVariables.HEIGHT / 2 - 45/*72*/,
                                Color.Black);

                            Simplify.LeftText(sb,
                                TheContentLoader.Font[0],
                                "8",
                                (TheContentLoader.Font[0].MeasureString("Level: 1").X) + 20/*95*/, VideoVariables.HEIGHT / 2 - 45/*72*/,
                                Color.Black);

                            if ((mouseState.X > (TheContentLoader.Font[0].MeasureString("Level: ").X) + 20 && mouseState.X < (TheContentLoader.Font[0].MeasureString("Level: 1").X) + 20) &&
                                (mouseState.Y > VideoVariables.HEIGHT / 2 - 45 - TheContentLoader.Font[0].MeasureString("1").Y && mouseState.Y < VideoVariables.HEIGHT / 2 - 40))
                            {
                                Simplify.LeftText(sb,
                                TheContentLoader.Font[0],
                                "1",
                                (TheContentLoader.Font[0].MeasureString("Level: ").X) + 20/*95*/, VideoVariables.HEIGHT / 2 - 45,
                                new Color(30, 255, 255));
                            }
                            else
                            {
                                Simplify.LeftText(sb,
                                TheContentLoader.Font[0],
                                "1",
                                (TheContentLoader.Font[0].MeasureString("Level: ").X) + 20, VideoVariables.HEIGHT / 2 - 45,
                                Color.Black);
                            }
                            #endregion
                        }
                        Simplify.LeftText(sb,
                                TheContentLoader.Font[0],
                                "Fails: " + deathCount,
                                20, VideoVariables.HEIGHT / 2 - 20/*47*/,
                                Color.Black);
                    }
                    else
                    {
                        Simplify.LeftText(sb,
                                TheContentLoader.Font[2],
                                "Level: " + (curQuestion + 1),
                                20, VideoVariables.HEIGHT / 2 - 45/*72*/,
                                Color.White);

                        Simplify.LeftText(sb,
                                TheContentLoader.Font[2],
                                "Fails: " + deathCount,
                                20, VideoVariables.HEIGHT / 2 - 20/*47*/,
                                Color.White);
                    }
                    if (curQuestion == 38)
                    {
                        if ((questions[38] as Question38).GetProcessWait())
                        {
                            if (muteSoundButton.Muted == 0)
                            {
                                TheContentLoader.Sound[0].Play();
                                //correctSound.Stop();
                                //correctSound.Play();
                            }
                            correct[curQuestion] += 7;
                            ++curQuestion;
                        }
                    }

                    questions[curQuestion].DisplayQuestion(sb);
                    questions[curQuestion].DisplayAnswers(sb, mouseState);
                    if (Final10)
                    {
                        muteSoundButton.Display666(sb, mouseState);
                        muteMusicButton.Display666(sb, mouseState);
                    }
                    else
                    {
                        muteSoundButton.Display(sb, mouseState);
                        muteMusicButton.Display(sb, mouseState);
                    }
                    break;
                #endregion

                #region Walls of Text
                case 2:
                    youWin.Show(sb, mouseState);
                    break;

                case 3:
                    credits.Show(sb, mouseState);
                    break;

                case 4:
                    instructions.Show(sb, mouseState);
                    break;
                #endregion

                #region Someone messed up
                default:
                    throw new Exception("You are stupid, there is no gameState value beyond 4");
                #endregion
            }

        }
        public void Update(KeyboardState keyboardState, MouseState mouseState, Game g)
        {
            KeyboardState oldKeyboardState = Controls.OldKeyboardState;
            MouseState oldMouseState = Controls.OldMouseState;

            if (Controls.FrameRates(keyboardState))
            {
                showFR = !showFR;
            }
            if (curQuestion == 38)
            {
                (questions[38] as Question38).AdvanceQuestion(keyboardState, oldKeyboardState);
            }
            if (keyWinBlade)
            {
                if (gameState == 1)
                {
                    if (keyboardState.IsKeyUp(Keys.Z) && oldKeyboardState.IsKeyDown(Keys.Z))
                    {
                        Correct();
                    }
                    if (keyboardState.IsKeyUp(Keys.C) && oldKeyboardState.IsKeyDown(Keys.C))
                    {
                        if (muteMusicButton.Muted == 0)
                        {
                            MediaPlayer.Play(TheContentLoader.Music[1]);
                        }
                        curQuestion = QUESTION_NUM - 1;
                    }
                }
            }

            if (mouseState.LeftButton == ButtonState.Released && oldMouseState.LeftButton == ButtonState.Pressed)
            {
                switch (gameState)
                {
                    #region Title Screen
                    case 0:
                        muteSoundButton.Clicked(mouseState);
                        muteMusicButton.Clicked(mouseState);
                        switch (title.ClickAnswers(mouseState))
                        {
                            case 0:
                                if (muteSoundButton.Muted == 0)
                                {
                                    TheContentLoader.Sound[3].Play();
                                    //startSound.Stop();
                                    //startSound.Play();
                                }
                                if (muteMusicButton.Muted == 0)
                                {
                                    MediaPlayer.Play(TheContentLoader.Music[0]);
                                }

                                curQuestion = 0;
                                deathCount = 0;
                                for (int i = QUESTION_NUM - 1; i >= 0; --i)
                                {
                                    wrong[i] = 0;
                                }
                                for (int i = QUESTION_NUM - 1; i >= 0; --i)
                                {
                                    correct[i] = 0;
                                }
                                gameState = 1;
                                break;
                            case 1:
                                gameState = 3;
                                break;
                            case 2:
                                gameState = 4;
                                break;
                            case 3:
                                g.Exit();
                                break;
                        }
                        break;
                    #endregion

                    #region Quiz
                    case 1:
                        muteSoundButton.Clicked(mouseState);
                        if (muteMusicButton.Clicked(mouseState))
                        {
                            byte temp = muteMusicButton.Muted;
                            if (temp == 1)
                            {
                                MediaPlayer.Stop();
                            }
                            else if (temp == 0)
                            {
                                if (Final10)
                                {
                                    MediaPlayer.Play(TheContentLoader.Music[1]);
                                }
                                else
                                {
                                    MediaPlayer.Play(TheContentLoader.Music[0]);
                                }
                            }
                        }

                        byte temp2 = muteSoundButton.Muted;

                        if (temp2 == 1)
                        {
                            correctSound.Stop();
                            wrongSound.Stop();
                            final10Wrong.Stop();
                            startSound.Stop();
                            final10Sound.Stop();
                        }

                        switch (questions[curQuestion].ClickAnswers(mouseState))
                        {
                            case 1:
                                if (temp2 == 0)
                                {
                                    if (Final10)
                                    {
                                        TheContentLoader.Sound[2].Play();
                                        //final10Wrong.Stop();
                                        //final10Wrong.Play();
                                    }
                                    else
                                    {
                                        TheContentLoader.Sound[1].Play();
                                        //wrongSound.Stop();
                                        //wrongSound.Play();
                                    }
                                }
                                wrong[curQuestion] += 3;
                                ++deathCount;
                                break;
                            case 2:
                                Correct();
                                break;

                            case 128:
                                if (temp2 == 0)
                                {
                                    TheContentLoader.Sound[1].Play();
                                    //wrongSound.Stop();
                                    //wrongSound.Play();
                                }
                                wrong[curQuestion] += 3;
                                ++deathCount;
                                --curQuestion;
                                break;
                        }
                        break;
                    #endregion

                    #region Walls of Text
                    case 2:
                        if (youWin.ClickExit(mouseState))
                        {
                            gameState = 0;
                        }
                        break;

                    case 3:
                        if (credits.ClickExit(mouseState))
                        {
                            gameState = 0;
                        }
                        break;
                    case 4:
                        if (instructions.ClickExit(mouseState))
                        {
                            gameState = 0;
                        }
                        break;
                    #endregion
                }
            }


            //Must be at the end.
            Controls.OldState(keyboardState, mouseState);
        }

        public void Correct()
        {
            if (muteSoundButton.Muted == 0)
            {
                if (curQuestion != TURNOVER_NUM)
                {
                    TheContentLoader.Sound[0].Play();
                    //correctSound.Stop();
                    //correctSound.Play();
                }
                else
                {
                    //TheContentLoader.Sound[4].Play();
                    final10Sound.Play();
                }
            }
            if (curQuestion == TURNOVER_NUM)
            {
                if (muteMusicButton.Muted == 0)
                {
                    MediaPlayer.Play(TheContentLoader.Music[1]);
                    //backMusic.mute();

                    //final10Music.unmute();
                    //final10Music.rewind();
                    //final10Music.play();
                }
            }
            correct[curQuestion] = 7;
            ++curQuestion;
            if (curQuestion >= QUESTION_NUM)
            {
                youWin = new YouWin(correct, wrong, deathCount);
                MediaPlayer.Stop();
                gameState = 2;
                curQuestion = 0;
            }
        }

        public byte GameState { get { return gameState; } set { gameState = value; } }
        public bool Final10 { get { return curQuestion > TURNOVER_NUM; } }
        public MuteMusicButton MusicButton { get { return muteMusicButton; } }
        public MuteSoundButton SoundButton { get { return muteSoundButton; } }
    }
}

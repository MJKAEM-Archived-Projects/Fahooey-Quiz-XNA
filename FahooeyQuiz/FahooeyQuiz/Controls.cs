﻿#region Using Statements
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Input;
#endregion

namespace FahooeyQuiz
{
    public static class Controls
    {
        private static KeyboardState oldKeyboardState = Keyboard.GetState();
        private static MouseState oldMouseState = Mouse.GetState();

        public static void OldState(KeyboardState keyboardState, MouseState mouseState)
        {
            oldKeyboardState = keyboardState;
            oldMouseState = mouseState;
        }
        public static bool FrameRates(KeyboardState keyboardState)
        {
            if (keyboardState.IsKeyUp(Keys.OemTilde) && oldKeyboardState.IsKeyDown(Keys.OemTilde))
            {
                return true;
            }
            return false;
        }
        public static KeyboardState OldKeyboardState { get { return oldKeyboardState; } }
        public static MouseState OldMouseState { get { return oldMouseState; } }
    }
}

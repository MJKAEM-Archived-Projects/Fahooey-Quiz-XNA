#region Using Statements
using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using VideoDisplay;
using ContentLoader;
using Questions;
using ErrorDepartment;
#endregion

namespace FahooeyQuiz
{
    /// <summary>
    /// This is the main type for your game
    /// </summary>
    public class Main : Microsoft.Xna.Framework.Game
    {
        private GraphicsDeviceManager graphics;
        private SpriteBatch spriteBatch;

        private static GameControl gameControl;
        private MouseState mouseState;
        private KeyboardState keyboardState;

        private string version = "XNA.1.0.2013.12.23";


        public Main()
        {
            ConsolePrinter.BluePrint("Starting 'Fahooey Quiz' " + version + ".\n");
            Window.Title = "Fahooey Quiz " + version;

            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";

            gameControl = new GameControl();

            graphics.PreferredBackBufferWidth = VideoVariables.WIDTH;
            graphics.PreferredBackBufferHeight = VideoVariables.HEIGHT;

            IsMouseVisible = true;


            gameControl.Setup();
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            // TODO: Add your initialization logic here
            ConsolePrinter.BluePrint("Starting game with resolution: " + VideoVariables.WIDTH + 'x' + VideoVariables.HEIGHT + "\n");

            base.Initialize();
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            spriteBatch = new SpriteBatch(GraphicsDevice);

            // TODO: use this.Content to load your game content here
            TheContentLoader.LoadEverything(Content);

            if (TheContentLoader.Sound[0] != null)
            {
                gameControl.SetInstanceSounds();
            }
            else
            {
                gameControl.SoundButton.Muted = 2;
            }
            if (TheContentLoader.Music[0] == null)
            {
                gameControl.MusicButton.Muted = 2;
            }

            ConsolePrinter.CyanPrint("The game is ready!\n");
        }

        #region No one cares
        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// all content.
        /// </summary>
        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
        }
        #endregion

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            // Allows the game to exit
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed)
                this.Exit();

            // TODO: Add your update logic here
            if (IsActive)
            {
                mouseState = Mouse.GetState();
                keyboardState = Keyboard.GetState();
                gameControl.Update(keyboardState, mouseState, this as Game);
            }

            base.Update(gameTime);
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            // TODO: Add your drawing code here
            spriteBatch.Begin();

            gameControl.Draw(gameTime, GraphicsDevice, spriteBatch, mouseState);

            spriteBatch.End();

            base.Draw(gameTime);
        }
        public static GameControl GameCon { get { return gameControl; } }
    }
}
